/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shockwave.pdfium.demo.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * GetByteArray Converts the PDF to bytearray
 */
public class GetByteArray {
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;

    /**
     * getFileFromRawFile
     *
     * @param inputStream The Fileinputstream containing FD
     * @return buffer The byte buffer containing the PDF data.
     * @throws IOException Throws IOException incase of the FD is null
     */
    public static byte[] toByteArray(InputStream inputStream) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        int num = 0;
        while ((num = inputStream.read(buffer)) != -1) {
            os.write(buffer, 0, num);
        }
        return os.toByteArray();
    }
}
