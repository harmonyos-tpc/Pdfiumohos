/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shockwave.pdfium.demo.metadata;

import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.util.LogUtil;

import java.util.List;

/**
 * GetBookMark to return the Bookmarks from the document
 */
public class GetBookMark {
    private static final String TAG = GetBookMark.class.getName();

    /**
     * print the bookmark tree
     *
     * @param tree the bookmark tree from native
     */
    public static void printBookmarksTree(List<PdfDocument.Bookmark> tree) {
        for (PdfDocument.Bookmark bookmark : tree) {
            LogUtil.info(TAG, "BookMark:" + bookmark.getTitle() + " Page_Index:" + bookmark.getPageIdx());
            if (bookmark.hasChildren()) {
                printBookmarksTree(bookmark.getChildren());
            }
        }
    }
}
