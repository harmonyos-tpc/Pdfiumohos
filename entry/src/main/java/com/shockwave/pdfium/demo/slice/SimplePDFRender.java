/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shockwave.pdfium.demo.slice;

import com.shockwave.pdfium.demo.ResourceTable;
import com.shockwave.pdfium.demo.metadata.GetBookMark;
import com.shockwave.pdfium.demo.metadata.GetMetaData;
import com.shockwave.pdfium.demo.metadata.GetPageLinks;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.shockwave.pdfium.util.LogUtil;
import com.shockwave.pdfium.demo.util.GetFile;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

/**
 * SimplePDFRender extends AbilitySlice
 */
public class SimplePDFRender extends AbilitySlice {
    private static final String TAG = SimplePDFRender.class.getName();
    private static final String FILE_PATH = "entry/resources/rawfile/sample.pdf";
    private static final String FILE_NAME = "sample.pdf";
    List<PdfDocument.Link> links;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_pixelmaplayout);
        try {
            openPdf(getContext());
        } catch (IOException ex) {
            LogUtil.error(TAG, ex.getMessage());
        }
    }

    private void openPdf(Context ctx) throws IOException {
        Component ivComp = findComponentById(ResourceTable.Id_myimg);
        Image iv = null;
        if (ivComp instanceof  Image) {
            iv = (Image) ivComp;
        }
        if (iv != null) {
            int pageNum = 0;
            RawFileEntry rawFileEntry = getResourceManager().getRawFileEntry(FILE_PATH);
            File file = GetFile.getFileFromRawFile(FILE_NAME, rawFileEntry, getCacheDir());
            FileDescriptor fd = new RandomAccessFile(file, "r").getFD();
            PdfiumCore pdfiumCore = new PdfiumCore(ctx);
            try {
                PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
                pdfiumCore.openPage(pdfDocument, pageNum);

                PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
                initializationOptions.pixelFormat = PixelFormat.RGB_565;
                initializationOptions.size = new Size(1000, 2000);
                PixelMap pixelMap = PixelMap.create(initializationOptions);
                pdfiumCore.renderPageBitmap(pdfDocument, pixelMap, pageNum, 0, 0, 1000, 2000);
                iv.setPixelMap(pixelMap);

                int widthPoint = pdfiumCore.getPageWidthPoint(pdfDocument, pageNum);
                int heightPoint = pdfiumCore.getPageHeightPoint(pdfDocument, pageNum);
                int widthPixel = pdfiumCore.getPageWidth(pdfDocument, pageNum);
                int heightPixel = pdfiumCore.getPageHeight(pdfDocument, pageNum);
                int pageCount = pdfiumCore.getPageCount(pdfDocument);
                com.shockwave.pdfium.util.Size size = pdfiumCore.getPageSize(pdfDocument, pageNum);

                GetMetaData.printMetaData(pdfDocument, pdfiumCore);
                GetPageLinks.printPageLinks(pdfiumCore.getPageLinks(pdfDocument, pageNum));
                GetBookMark.printBookmarksTree(pdfiumCore.getTableOfContents(pdfDocument));

                LogUtil.info(TAG, "Page width in postscript points " + widthPoint);
                LogUtil.info(TAG, "Page height in postscript points " + heightPoint);
                LogUtil.info(TAG, "Page width in pixels " + widthPixel);
                LogUtil.info(TAG, "Page height in pixels " + heightPixel);
                LogUtil.info(TAG, "Page Count " + pageCount);
                LogUtil.info(TAG, "Page Size in Pixels " + size.getWidth() + " " + size.getHeight());

                links = pdfiumCore.getPageLinks(pdfDocument, pageNum);
                for (PdfDocument.Link link : links) {
                    RectFloat mappedRect = pdfiumCore.mapRectToDevice(pdfDocument, pageNum,
                            0, 0, widthPoint, heightPoint, 0, link.getBounds());
                    LogUtil.info(TAG, "Link " + link.getUri() + " Mapped to page_coord ["
                            + mappedRect.toString() + "]");
                }
                pdfiumCore.closeDocument(pdfDocument); // important!

            } catch(IOException ex) {
                LogUtil.error(TAG, ex.getMessage());
            }
        }
    }
}
