/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shockwave.pdfium.demo.slice;

import com.shockwave.pdfium.demo.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * MainAbilitySlice.
 */
public class MainAbilitySlice extends AbilitySlice {
    Component loadpixelmapComp;
    Component multidocComp;
    Component pswddocComp;
    Component bytedocComp;
    Component desireddocComp;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initializeButtons();
        initializeListeners();
    }

    /**
     * initializeButtons To initiliaze view Buttons
     */
    public void initializeButtons() {
        loadpixelmapComp = findComponentById(ResourceTable.Id_load_pixelmap_doc);
        multidocComp = findComponentById(ResourceTable.Id_load_multi_page);
        pswddocComp = findComponentById(ResourceTable.Id_load_pswd_doc);
        bytedocComp = findComponentById(ResourceTable.Id_load_byte_doc);
        desireddocComp = findComponentById(ResourceTable.Id_load_desired_doc);
    }

    /**
     * initializeListeners To initiliaze view components
     */
    public void initializeListeners() {
        if (loadpixelmapComp instanceof  Button) {
            Button loadpixelmap = (Button) loadpixelmapComp;
            loadpixelmap.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    present(new SimplePDFRender(), new Intent());
                }
            });
        }
        if (multidocComp instanceof  Button) {
            Button multidoc = (Button) multidocComp;
            multidoc.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    present(new LoadAllPages(), new Intent());
                }
            });
        }

        if (desireddocComp instanceof  Button) {
            Button desireddoc = (Button) desireddocComp;
            desireddoc.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    present(new LoadDesiredPage(), new Intent());
                }
            });
        }

        if (pswddocComp instanceof Button) {
            Button pswddoc = (Button) pswddocComp;
            pswddoc.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    present(new PasswordPDFRender(), new Intent());
                }
            });
        }
        if (bytedocComp instanceof Button) {
            Button bytedoc = (Button) bytedocComp;
            bytedoc.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    present(new ByteDataDocument(), new Intent());
                }
            });
        }
    }
}
