/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shockwave.pdfium.demo.metadata;

import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.shockwave.pdfium.util.LogUtil;

/**
 * GetMetaData to return the MetaData from the document
 */
public class GetMetaData {
    private static final String TAG = GetMetaData.class.getName();

    /**
     * print the bookmark tree
     *
     * @param pdfDocument the opened pdfdocument
     * @param pdfiumCore the pdfdocument's core
     */
    public static void  printMetaData(PdfDocument pdfDocument, PdfiumCore pdfiumCore) {
            PdfDocument.Meta meta = pdfiumCore.getDocumentMeta(pdfDocument);
            LogUtil.info(TAG, "Title = " + meta.getTitle());
            LogUtil.info(TAG, "author = " + meta.getAuthor());
            LogUtil.info(TAG, "subject = " + meta.getSubject());
            LogUtil.info(TAG, "keywords = " + meta.getKeywords());
            LogUtil.info(TAG, "creator = " + meta.getCreator());
            LogUtil.info(TAG, "producer = " + meta.getProducer());
            LogUtil.info(TAG, "creationDate = " + meta.getCreationDate());
            LogUtil.info(TAG, "modDate = " + meta.getModDate());
    }
}
