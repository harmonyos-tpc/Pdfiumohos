/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shockwave.pdfium.demo.metadata;

import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.util.LogUtil;

import java.util.List;

/**
 * GetPageLinks to return the PageLinks from the document
 */
public class GetPageLinks {
    private static final String TAG = GetPageLinks.class.getName();

    /**
     * print the page links
     *
     * @param tree The Pdfdocument's Page links
     */
    public static void printPageLinks(List<PdfDocument.Link> tree) {
        for (PdfDocument.Link link:tree) {
            if (link.getDestPageIdx() == null) {
                LogUtil.info(TAG, "Page Link " + link.getUri());
            }
        }
    }
}
