package com.shockwave.pdfium.demo;

import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.shockwave.pdfium.demo.util.GetFile;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;

import static org.junit.Assert.assertTrue;

/**
 * SamplePdfTest.
 */
public class SamplePdfTest {
    private static final String FILE_PATH = "entry/resources/rawfile/sample.pdf";
    private static final String FILE_NAME = "sample.pdf";
    PdfiumCore pdfiumCore;
    PdfDocument pdfDocument;
    private int pageNum = 0;
    private int pageCount = 0;
    private int pageHeightPoint = 0;
    private int pageWidthPoint = 0;
    private int heightPixel = 0;
    private int widthPixel = 0;
    private Context context;

    /**
     * setup for testCases
     * @throws IOException
     */
    @Before
    public void setup() throws IOException {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry(FILE_PATH);
        File file = GetFile.getFileFromRawFile(FILE_NAME, rawFileEntry, context.getCacheDir());
        FileDescriptor fd = new RandomAccessFile(file, "r").getFD();
        pdfiumCore = new PdfiumCore(context);
        pdfDocument = pdfiumCore.newDocument(fd);
        pdfiumCore.openPage(pdfDocument, pageNum, pdfiumCore.getPageCount(pdfDocument) - 1);
        pageCount = pdfiumCore.getPageCount(pdfDocument);
        pageHeightPoint = pdfiumCore.getPageHeightPoint(pdfDocument, pageNum);
        pageWidthPoint = pdfiumCore.getPageWidthPoint(pdfDocument, pageNum);
        heightPixel = pdfiumCore.getPageHeight(pdfDocument, pageNum);
        widthPixel = pdfiumCore.getPageWidth(pdfDocument, pageNum);
        pdfiumCore.closeDocument(pdfDocument);
    }

    /**
     * testPageCount
     */
    @Test
    public void testPageCount() {
        assertTrue(pageCount > 0);
    }

    /**
     * testPageCount
     */
    @Test
    public void testPageHeightPoint() {
        assertTrue(pageHeightPoint > 0);
    }

    /**
     * testPageCount
     */
    @Test
    public void testPageWidthPoint() {
        assertTrue(pageWidthPoint > 0);
    }

    /**
     * testPageCount
     */
    @Test
    public void testPageHeight() {
        assertTrue(heightPixel > 0);
    }

    /**
     * testPageCount
     */
    @Test
    public void testPageWidth() {
        assertTrue(widthPixel > 0);
    }
}
