package com.shockwave.pdfium.demo;

import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.shockwave.pdfium.demo.util.GetFile;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.global.resource.RawFileEntry;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;

import static org.junit.Assert.assertNotNull;

/**
 * MetaDataTest.
 */
public class MetaDataTest {
    private static final String FILE_PATH = "entry/resources/rawfile/sample.pdf";
    private static final String FILE_NAME = "sample.pdf";
    PdfDocument.Meta meta = null;
    PdfiumCore pdfiumCore;
    PdfDocument pdfDocument;
    private Context context;
    private int pageNum = 0;

    /**
     * setup for testCases
     * @throws IOException
     */
    @Before
    public void setup() throws IOException {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        RawFileEntry rawFileEntry = context.getResourceManager().getRawFileEntry(FILE_PATH);
        File file = GetFile.getFileFromRawFile(FILE_NAME, rawFileEntry, context.getCacheDir());
        FileDescriptor fd = new RandomAccessFile(file, "r").getFD();
        pdfiumCore = new PdfiumCore(context);
        pdfDocument = pdfiumCore.newDocument(fd);
        pdfiumCore.openPage(pdfDocument, pageNum, pdfiumCore.getPageCount(pdfDocument) - 1);
        meta = pdfiumCore.getDocumentMeta(pdfDocument);
        pdfiumCore.closeDocument(pdfDocument);
    }

    /**
     * titleTest
     */
    @Test
    public void titleTest() {
        assertNotNull(meta.getTitle());
    }

    /**
     * authorTest
     */
    @Test
    public void authorTest() {
        assertNotNull(meta.getAuthor());
    }

    /**
     * subjectTest
     */
    @Test
    public void subjectTest() {
        assertNotNull(meta.getSubject());
    }

    /**
     * keywordsTest
     */
    @Test
    public void keywordsTest() {
        assertNotNull(meta.getKeywords());
    }

    /**
     * creatorTest
     */
    @Test
    public void creatorTest() {
        assertNotNull(meta.getCreator());
    }

    /**
     * producerTest
     */
    @Test
    public void producerTest() {
        assertNotNull(meta.getProducer());
    }

    /**
     * dateTest
     */
    @Test
    public void dateTest() {
        assertNotNull(meta.getCreationDate());
    }

    /**
     * moddateTest
     */
    @Test
    public void moddateTest() {
        assertNotNull(meta.getModDate());
    }
}
