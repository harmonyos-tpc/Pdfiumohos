# Pdfiumohos

Pdfium is a Pixelmap rendering solution that helps to perform primitive operations on PDF document

## Installation Tutorial:

1. To build with the Pdfium library code:
        Sample app changes:
                dependencies {
                        implementation project(':pdfium')
                }

2. To build with Pdfium.har file
        Sample app changes:
        add Pdfium.har in entry/libs folder
        add the follwing dependency in entry build.gradle file
                dependencies {
                        implementation files('libs/pdfium.har')
                }

3. Add the dependent shared libraries libmodft2.so, libmodpdfium.so,libmodpng.so, libc++_shared.so in pdfium LIBRARY to generate libjniPdfium.so
        add_library(LIB_NAME SHARED IMPORTED)
        set_target_properties(LIB_NAME
                       PROPERTIES IMPORTED_LOCATION
                                           ${CMAKE_CURRENT_SOURCE_DIR}/lib/arm64-v8a/LIB_NAME.so)

   Finally add the libjniPdfium.so in sample's /libs/arm64-v8a (Create the folder if not present) in both cases.

4.For using Pdfiumohos from a remote repository in separate application, add the below dependency in entry/build.gradle file.
   
   Modify entry build.gradle as below :
               
               
               ```gradle
               dependencies {
                      implementation 'io.openharmony.tpc.thirdlib:Pdfiumohos:1.0.0'
               }
               ```

## Usage Instructions

A sample project which provides runnable code examples that demonstrate uses of the classes in this project is available in the entry/ folder.

The following core classes are the essential interface to Pdfium:

Pdfium - Helps in rendering the pdf document on to the pixelmap component along with supporting the primitive operations

1. Create the PdfiumCore with current Ability context and Pdfdocument based on the input source.
                PdfiumCore pdfiumCore = new PdfiumCore(context);
                PdfDocument pdfDocument = pdfiumCore.newDocument(fd); //RawFile
                PdfDocument pdfDocument = pdfiumCore.newDocument(fd, FILE_PASSWORD); //RawFile_with_Password
                PdfDocument pdfDocument = pdfiumCore.newDocument(byteArray); //RawFile_into_ByteArray
                
2. Open the PDF Document page along with TO: and FROM: Indices
                pdfiumCore.openPage(pdfDocument, pageNum); //SinglePage
                pdfiumCore.openPage(pdfDocument, Start, End); //Mutiple_Pages

3. Obtain the Height and Width to Render on the Pixel Map Component
                PageWidth = pdfiumCore.getPageWidthPoint(pdfDocument, pageNum);
                PageHeight = pdfiumCore.getPageHeightPoint(pdfDocument, pageNum);
                Size size = new Size (PageWidth, PageHeight);

4. Render the PDF Page on the PixelMap component.
                pdfiumCore.renderPageBitmap(pdfDocument, pixelMap, pageNum, StartX, StartY, PageWidth, PageHeight);
                setPixelMap(pixelMap);

5. Obtain the Metadata and other primitive informations of the PDF Document
                GetMetaData.printMetaData(pdfDocument, pdfiumCore);
                GetPageLinks.printPageLinks(pdfiumCore.getPageLinks(pdfDocument, pageNum));
                GetBookMark.printBookmarksTree(pdfiumCore.getTableOfContents(pdfDocument));

6. Closing or clearing all the resources.
                pdfiumCore.closeDocument(pdfDocument);


NOTE: All the functionalities are demonstarted with pixelMap in Directional layout
 
-----------------
## Features
* Create the pdfiumcore and pdfdocument based on the context
* Open or load the PDF page
* Obtaining the necessary information to render on the component
* Render the object on the PixelMap 
* Obtain the Metadata information of the document.
* Close the resources.

-----------------
## Limitation
* Rendering the pdf page on the Surface object is not supported yet