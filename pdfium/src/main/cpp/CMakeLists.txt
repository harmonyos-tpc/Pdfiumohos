# the minimum version of CMake.
cmake_minimum_required(VERSION 3.4.1)
project(jniPdfium)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include)
set(CMAKE_C_FLAGS "-std=c99 -Wno-typedef-redefinition")

set(CMAKE_THREAD_PREFER_PTHREAD ON)
set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
if(CMAKE_USE_PTHREADS_INIT)
        set(HAVE_PTHREADS TRUE)
        add_definitions(-DHAVE_PTHREADS)
endif()

add_library(jniPdfium SHARED src/mainJNILib.cpp)

add_library(libmodpdfium SHARED IMPORTED)
set_target_properties(libmodpdfium
                       PROPERTIES IMPORTED_LOCATION
                                           ${CMAKE_CURRENT_SOURCE_DIR}/lib/arm64-v8a/libmodpdfium.so)

add_library(libmodft2 SHARED IMPORTED)
set_target_properties(libmodft2
                       PROPERTIES IMPORTED_LOCATION
                                           ${CMAKE_CURRENT_SOURCE_DIR}/lib/arm64-v8a/libmodft2.so)

add_library(libmodpng SHARED IMPORTED)
set_target_properties(libmodpng
                       PROPERTIES IMPORTED_LOCATION
                                           ${CMAKE_CURRENT_SOURCE_DIR}/lib/arm64-v8a/libmodpng.so)

add_library(libc++_shared SHARED IMPORTED)
set_target_properties(libc++_shared
                       PROPERTIES IMPORTED_LOCATION
                                           ${CMAKE_CURRENT_SOURCE_DIR}/lib/arm64-v8a/libc++_shared.so)


target_link_libraries(
                       jniPdfium
                       libmodpdfium
                       libmodpng
                       libmodft2
                       libc++_shared
                       image_pixelmap.z
                       hilog_ndk.z
                       zgraphic.z
                                           )